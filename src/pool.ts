export interface Poolable {
  poolId?: number;
  isAlive?: boolean;
  cleanup: () => void;
  reset: () => void;
}

export class Pool<T extends Poolable> {
  objs: T[] = [];
  private availableIds: number[] = [];
  private createFunc: () => T;
  private currentSize = 0;

  constructor(createFunc: () => T, initialSize: number = 100) {
    this.createFunc = createFunc;

    for (let i = 0; i < initialSize; i++) {
      const el = this.createFunc();
      el.poolId = i;
      el.isAlive = false;
      this.objs[i] = el;
      this.availableIds.push(i);
    }

    this.currentSize = initialSize;
  }

  obtain = (): T => {
    if (this.availableIds.length > 0) {
      const poolId = this.availableIds.pop();
      const obj = this.objs[poolId];

      obj.isAlive = true;
      obj.reset();

      return obj;
    } else {
      const obj = this.createFunc();

      obj.isAlive = true;
      obj.poolId = this.currentSize;
      obj.reset();

      this.objs.push(obj);

      this.currentSize++;

      return obj;
    }
  };

  free = (obj: T) => {
    if (!obj.isAlive) return;

    obj.isAlive = false;

    this.availableIds.push(obj.poolId);

    obj.cleanup();
  };

  clear = () => {
    for (let i = 0; i < this.objs.length; i++) {
      this.free(this.objs[i]);
    }
  };

  activeObjs = () => this.objs.filter(obj => obj.isAlive);

  destroy = () => {
    this.activeObjs = null;
    this.availableIds = null;
  };
}
