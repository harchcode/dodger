import { KeyCode } from './constants';

let isPressed: Record<string, boolean> = {};

function handleKeyDown(e: KeyboardEvent) {
  isPressed[e.key] = true;
}

function handleKeyUp(e: KeyboardEvent) {
  isPressed[e.key] = false;
}

export function addKeyboardInputListener() {
  window.addEventListener('keydown', handleKeyDown);
  window.addEventListener('keyup', handleKeyUp);
}

export function removeKeyboardInputListener() {
  window.removeEventListener('keydown', handleKeyDown);
  window.removeEventListener('keyup', handleKeyDown);
  isPressed = {};
}

export function isKeyPressed(code: KeyCode) {
  if (code === KeyCode.ANY) {
    return Object.values(isPressed).findIndex(p => p) > -1;
  }

  return isPressed[code] ? isPressed[code] : false;
}
