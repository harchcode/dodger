import { Scene } from './scene';
import { Container } from 'pixi.js';
import { Game } from './game';
import { Pool } from './pool';
import { Enemy } from './enemy';
import { randInt } from './utils';
import { GameState, MAX_LIFE, PlayerState } from './constants';
import levels from './levels';
import { PlayScene } from './play-scene';
import { Player } from './player';

export class GameScene implements Scene {
  container: Container;
  game: Game;
  playScene: PlayScene;

  player: Player;
  enemies: Pool<Enemy>;
  currentLevel = 0;
  spawnCounter = 0;
  score = 0;
  life = 3;

  constructor(game: Game) {
    this.game = game;
    this.playScene = game.playScene;
  }

  start = () => {
    this.container = new Container();
    this.game.app.stage.addChild(this.container);

    this.enemies = new Pool<Enemy>(() => new Enemy());
    this.player = new Player();
    this.container.addChild(this.player.sprite);
  };

  reset = () => {
    this.currentLevel = 0;
    this.score = 0;
    this.life = MAX_LIFE;
    this.enemies.clear();

    this.player.spawn();
  };

  update = (dt: number) => {
    const game = this.game;

    if (game.state < GameState.PLAY) return;

    const level = levels[this.currentLevel];

    this.player.handleInput();
    this.player.update(dt);

    this.enemies.activeObjs().forEach(e => {
      e.update(dt);

      if (e.duration < e.targetDuration) return;

      this.container.removeChild(e.sprite);
      this.enemies.free(e);

      if (game.state !== GameState.PLAY) return;

      this.score++;

      this.playScene.scoreText.text = this.score.toString();

      const nextLevel = levels[this.currentLevel + 1];

      if (nextLevel && this.score >= nextLevel.fromScore) {
        this.currentLevel++;
      }
    });

    this.enemies.activeObjs().forEach(e => {
      this.checkCollision(e);
    });

    this.spawnCounter += dt;

    if (this.spawnCounter < level.spawnDelay) return;

    this.spawnCounter -= level.spawnDelay;

    for (let i = 0; i < level.spawnCount; i++) {
      const tmp = this.enemies.obtain();
      tmp.setSpeed(randInt(level.minEnemySpeed, level.maxEnemySpeed));
      this.container.addChild(tmp.sprite);
    }
  };

  end = () => {
    this.game.app.stage.removeChild(this.container);
    this.container.destroy();
  };

  checkCollision = (enemy: Enemy) => {
    const player = this.player;

    const rtot = player.r + enemy.r;
    const dx = enemy.sprite.x - player.sprite.x;
    const dy = enemy.sprite.y - player.sprite.y;
    const lsq = dx * dx + dy * dy;

    if (lsq <= rtot * rtot) {
      if (player.state === PlayerState.VULNERABLE) {
        this.life -= 1;
        this.playScene.setLifeDisplay(this.life);

        if (this.life > 0) {
          this.player.onHurt();
        } else {
          this.player.onDead();
          this.onGameOver();
        }
      }
    }
  };

  onGameOver = () => {
    this.game.changeScene(this.game.gameoverScene);
  };
}
