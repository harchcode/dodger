import { Scene } from './scene';
import { Container, TextStyle, Text, Texture, Sprite } from 'pixi.js';
import { Game } from './game';
import { GAME_WIDTH, GAME_HEIGHT, GameState, MAX_LIFE } from './constants';
import { idleFrames } from './assets';

export class PlayScene implements Scene {
  container: Container;
  game: Game;

  scoreText: Text;
  lifeTexture: Texture;
  lifeSprites: Sprite[] = [];

  constructor(game: Game) {
    this.game = game;
  }

  start = () => {
    this.container = new Container();
    this.game.app.stage.addChild(this.container);
    this.lifeTexture = idleFrames[0];
    this.lifeSprites = [];

    const scoreLabelText = new Text(
      'Score:',
      new TextStyle({
        fontSize: 24,
        fill: '#fff'
      })
    );

    scoreLabelText.position.set(16, 16);
    this.container.addChild(scoreLabelText);

    const scoreText = new Text(
      '0',
      new TextStyle({
        fontSize: 24,
        fill: '#fff'
      })
    );

    scoreText.position.set(96, 16);
    this.scoreText = scoreText;
    this.container.addChild(scoreText);

    for (let i = 0; i < MAX_LIFE; i++) {
      const sprite = new Sprite(this.lifeTexture);

      this.lifeSprites.push(sprite);

      sprite.position.set(GAME_WIDTH - 24 - (sprite.width - 24) * (i + 1), -20);

      this.container.addChild(sprite);
    }
  };

  update = (dt: number) => {};

  setLifeDisplay(life: number) {
    for (let i = MAX_LIFE; i > life; i--) {
      this.lifeSprites[i - 1].visible = false;
    }

    for (let i = life; i > 0; i--) {
      this.lifeSprites[i - 1].visible = true;
    }
  }

  end = () => {
    this.game.app.stage.removeChild(this.container);
    this.container.destroy();
  };

  private onClick = () => {
    this.game.setState(GameState.PLAY);
    this.game.changeScene(null);
  };

  handleInput = () => {};
}
