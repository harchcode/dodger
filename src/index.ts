import { Application } from 'pixi.js';
import { Game } from './game';
import { GAME_WIDTH, GAME_HEIGHT, ASPECT_RATIO } from './constants';

async function main() {
  const gameContainer = document.getElementById('game');

  const app = new Application({
    backgroundColor: 0x744210,
    autoDensity: true,
    width: GAME_WIDTH,
    height: GAME_HEIGHT
  });

  app.renderer.view.style.width = '100%';
  app.renderer.view.style.height = '100%';

  const resize = () => {
    if (window.innerWidth > window.innerHeight * ASPECT_RATIO) {
      gameContainer.style.height = `${window.innerHeight}px`;
      gameContainer.style.width = `${window.innerHeight * ASPECT_RATIO}px`;
    } else {
      gameContainer.style.width = `${window.innerWidth}px`;
      gameContainer.style.height = `${window.innerWidth / ASPECT_RATIO}px`;
    }
  };

  resize();
  window.addEventListener('resize', resize);

  gameContainer.appendChild(app.view);

  const game = new Game(app);
  game.start();
}

main();
