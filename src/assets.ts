import spriteSheetData from './assets/images/spritesheet.json';
import spriteSheetImage from './assets/images/spritesheet.png';
import { Spritesheet, BaseTexture, Loader, Texture } from 'pixi.js';

const loader = Loader.shared;

export const deadFrames: Texture[] = [];
export const hurtFrames: Texture[] = [];
export const idleFrames: Texture[] = [];
export const spawnFrames: Texture[] = [];
export const walkFrames: Texture[] = [];
export const batFrames: Texture[] = [];
export const snakeFrames: Texture[] = [];

export function loadAll(): Promise<void> {
  return new Promise((resolve, reject) => {
    loader.add(spriteSheetImage).load(() => {
      const sheet = new Spritesheet(
        (loader.resources[spriteSheetImage].texture as unknown) as BaseTexture,
        spriteSheetData
      );

      sheet.parse(() => {
        for (let i = 0; i < 8; i++) {
          deadFrames.push(Texture.from(`sheet_hero_dead_${i + 1}.png`));
        }

        for (let i = 0; i < 4; i++) {
          hurtFrames.push(Texture.from(`sheet_hero_hurt_${i + 1}.png`));
        }

        for (let i = 0; i < 8; i++) {
          idleFrames.push(Texture.from(`sheet_hero_idle_${i + 1}.png`));
        }

        for (let i = 0; i < 6; i++) {
          spawnFrames.push(Texture.from(`sheet_hero_spawn_${i + 1}.png`));
        }

        for (let i = 0; i < 3; i++) {
          walkFrames.push(Texture.from(`sheet_hero_walk_${i + 1}.png`));
        }

        for (let i = 0; i < 4; i++) {
          batFrames.push(Texture.from(`sheet_bat_fly_${i + 1}.png`));
        }

        for (let i = 0; i < 7; i++) {
          snakeFrames.push(Texture.from(`sheet_snake_walk_${i + 1}.png`));
        }

        resolve();
      });
    });
  });
}

export default {
  loadAll
};
