interface Level {
  fromScore: number;
  spawnDelay: number;
  minEnemySpeed: number;
  maxEnemySpeed: number;
  spawnCount: number;
}

const levels: Level[] = [
  {
    fromScore: 0,
    spawnDelay: 1000,
    spawnCount: 2,
    minEnemySpeed: 100,
    maxEnemySpeed: 100
  },
  {
    fromScore: 30,
    spawnDelay: 500,
    spawnCount: 2,
    minEnemySpeed: 50,
    maxEnemySpeed: 150
  },
  {
    fromScore: 1000,
    spawnDelay: 500,
    spawnCount: 10,
    minEnemySpeed: 50,
    maxEnemySpeed: 500
  }
];

export default levels;
