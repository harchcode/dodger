import { AnimatedSprite, Texture } from 'pixi.js';
import { Poolable } from './pool';
import { randInt } from './utils';
import {
  ENEMY_WIDTH,
  ENEMY_HEIGHT,
  GAME_WIDTH,
  GAME_HEIGHT,
  EnemyDirection
} from './constants';
import { snakeFrames, batFrames } from './assets';

enum DisplayType {
  SNAKE = 1,
  BAT
}

export class Enemy implements Poolable {
  vx = 0;
  vy = 0;
  r = 8;

  startX = 0;
  targetX = 0;
  startY = 0;
  targetY = 0;
  speed = 0;
  duration = 0;
  targetDuration = 0;
  displayType: DisplayType;
  sprite: AnimatedSprite;
  direction: EnemyDirection;
  frames: Texture[];

  constructor() {}

  cleanup = () => {
    this.sprite.stop();
    this.sprite.visible = false;
  };

  reset = () => {
    this.duration = 0;
    this.displayType = randInt(2, 2);

    this.frames = this.displayType === 1 ? snakeFrames : batFrames;

    if (this.sprite) {
      this.sprite.textures = this.frames;
    } else {
      this.sprite = new AnimatedSprite(this.frames);
      this.sprite.width = ENEMY_WIDTH;
      this.sprite.height = ENEMY_HEIGHT;
      this.sprite.anchor.set(0.5, 0.5);
    }

    this.sprite.visible = true;

    this.direction = randInt(1, 4);

    const ew2 = ENEMY_WIDTH * 0.5;
    const eh2 = ENEMY_HEIGHT * 0.5;

    if (this.direction === EnemyDirection.TOP_TO_BOTTOM) {
      this.startX = randInt(ew2, GAME_WIDTH - ew2);
      this.startY = -eh2;
      this.targetX = randInt(ew2, GAME_WIDTH - ew2);
      this.targetY = GAME_HEIGHT + eh2;
    } else if (this.direction === EnemyDirection.RIGHT_TO_LEFT) {
      this.startX = GAME_WIDTH + ew2;
      this.startY = randInt(eh2, GAME_HEIGHT - eh2);
      this.targetX = -ew2;
      this.targetY = randInt(eh2, GAME_HEIGHT - eh2);
    } else if (this.direction === EnemyDirection.BOTTOM_TO_TOP) {
      this.startX = randInt(ew2, GAME_WIDTH - ew2);
      this.startY = GAME_HEIGHT + eh2;
      this.targetX = randInt(ew2, GAME_WIDTH - ew2);
      this.targetY = -eh2;
    } else if (this.direction === EnemyDirection.LEFT_TO_RIGHT) {
      this.startX = -ew2;
      this.startY = randInt(eh2, GAME_HEIGHT - eh2);
      this.targetX = GAME_WIDTH + ew2;
      this.targetY = randInt(eh2, GAME_HEIGHT - eh2);
    }

    const scaleX = ENEMY_WIDTH / this.frames[0].width;
    const scaleY = ENEMY_HEIGHT / this.frames[0].height;

    if (this.startX < this.targetX) {
      this.sprite.scale.set(-scaleX, scaleY);
    } else {
      this.sprite.scale.set(scaleX, scaleY);
    }

    this.sprite.position.set(this.startX, this.startY);
    this.sprite.play();
  };

  setSpeed(speed: number) {
    this.speed = speed;

    const tx = this.targetX - this.startX;
    const ty = this.targetY - this.startY;

    const len = Math.sqrt(tx * tx + ty * ty);

    this.vx = (tx / len) * speed;
    this.vy = (ty / len) * speed;

    this.duration = 0;
    this.targetDuration = len / speed;

    this.sprite.animationSpeed = speed * 0.001;
  }

  update(dt: number) {
    const sec = dt * 0.001;

    this.duration += sec;

    this.sprite.x += this.vx * sec;
    this.sprite.y += this.vy * sec;
  }
}
