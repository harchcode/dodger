import { Application, Ticker } from 'pixi.js';
import assets from './assets';
import { GameState } from './constants';
import { Scene } from './scene';
import { TitleScene } from './title-scene';
import { GameScene } from './game-scene';
import { PlayScene } from './play-scene';
import { GameoverScene } from './gameover-scene';
import { addKeyboardInputListener } from './input';

const ticker = Ticker.shared;

export class Game {
  app: Application;
  state = GameState.LOADING;

  titleScene = new TitleScene(this);
  playScene = new PlayScene(this);
  gameoverScene = new GameoverScene(this);
  gameScene = new GameScene(this);
  currentScene: Scene;

  constructor(app: Application) {
    this.app = app;
  }

  changeScene = (newScene: Scene) => {
    if (this.currentScene) this.currentScene.end();
    this.currentScene = newScene;
    if (this.currentScene) this.currentScene.start();
  };

  setState = (newState: GameState) => {
    this.state = newState;

    if (this.state === GameState.PLAY) {
      this.gameScene.reset();
    }
  };

  start = async () => {
    await assets.loadAll();

    this.gameScene.start();
    this.changeScene(this.titleScene);

    this.state = GameState.TITLE;

    this.app.ticker.add(() => this.update(ticker.deltaMS));

    addKeyboardInputListener();
  };

  update = (dt: number) => {
    this.gameScene.update(dt);
    if (this.currentScene) this.currentScene.update(dt);
  };
}
