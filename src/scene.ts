import { Container } from 'pixi.js';
import { Game } from './game';

export interface Scene {
  container: Container;
  game: Game;

  start: () => void;
  update: (dt: number) => void;
  end: () => void;
}
