import { Scene } from './scene';
import { Container, TextStyle, Text } from 'pixi.js';
import { Game } from './game';
import { GAME_WIDTH, GAME_HEIGHT, GameState, KeyCode } from './constants';
import { isKeyPressed } from './input';

export class GameoverScene implements Scene {
  container: Container;
  game: Game;

  helpText: Text;

  counter = 0;
  isEnabled = false;

  constructor(game: Game) {
    this.game = game;
  }

  start = () => {
    this.isEnabled = false;

    this.container = new Container();
    this.game.app.stage.addChild(this.container);

    const titleText = new Text(
      'Game Over',
      new TextStyle({
        fontSize: 48,
        fill: '#1A202C',
        stroke: '#2B6CB0',
        strokeThickness: 8,
        dropShadow: true,
        dropShadowColor: '#F7FAFC',
        dropShadowBlur: 4,
        dropShadowAngle: Math.PI / 2,
        dropShadowDistance: 2
      })
    );

    titleText.anchor.set(0.5, 0.5);
    titleText.scale.set(1.2, 1);
    titleText.position.set(GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5 - 80);
    this.container.addChild(titleText);

    const scoreText = new Text(
      `Your score: ${this.game.gameScene.score}`,
      new TextStyle({
        fontSize: 24,
        fill: '#fff'
      })
    );

    scoreText.anchor.set(0.5, 0.5);
    scoreText.position.set(GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5);
    this.helpText = scoreText;
    this.container.addChild(scoreText);

    const helpText = new Text(
      'Press any key to start',
      new TextStyle({
        fontSize: 24,
        fill: '#fff'
      })
    );

    helpText.anchor.set(0.5, 0.5);
    helpText.position.set(GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5 + 96);
    this.helpText = helpText;

    setTimeout(() => {
      this.container.addChild(helpText);
      this.isEnabled = true;
    }, 1000);
  };

  update = (dt: number) => {
    if (this.isEnabled && isKeyPressed(KeyCode.ANY)) this.startPlaying();

    this.counter += dt;

    if (this.helpText.visible && this.counter > 1000) {
      this.counter -= 1000;
      this.helpText.visible = false;
    } else if (!this.helpText.visible && this.counter > 200) {
      this.counter -= 200;
      this.helpText.visible = true;
    }
  };

  end = () => {
    this.game.app.stage.removeChild(this.container);
    this.container.destroy();
  };

  private startPlaying = () => {
    this.game.changeScene(this.game.playScene);
    this.game.setState(GameState.PLAY);
  };
}
