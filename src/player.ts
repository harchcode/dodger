import { AnimatedSprite, Texture } from 'pixi.js';
import {
  PlayerState,
  GAME_WIDTH,
  GAME_HEIGHT,
  PLAYER_WIDTH,
  PLAYER_HEIGHT,
  KeyCode
} from './constants';
import { isKeyPressed } from './input';
import { trunc } from './utils';
import {
  idleFrames,
  spawnFrames,
  walkFrames,
  hurtFrames,
  deadFrames
} from './assets';

export class Player {
  dirX = 0;
  dirY = 0;
  speed: number;
  sprite: AnimatedSprite;
  frames: Texture[];
  state = PlayerState.INVINCIBLE;
  r = 16;
  counter = 0;
  toggle = true;

  constructor() {
    this.speed = 200;

    this.sprite = new AnimatedSprite(idleFrames);
    this.sprite.anchor.set(0.5, 0.75);
    this.sprite.width = PLAYER_WIDTH;
    this.sprite.height = PLAYER_HEIGHT;
    this.sprite.animationSpeed = 0.1;
    this.sprite.position.set(GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5);
    this.sprite.play();
  }

  spawn = () => {
    this.speed = 200;
    this.sprite.position.set(GAME_WIDTH * 0.5, GAME_HEIGHT * 0.5);
    this.sprite.textures = spawnFrames;
    this.sprite.play();
    this.state = PlayerState.VULNERABLE;
  };

  setSpeed = (speed: number) => {
    this.speed = speed;
  };

  update = (dt: number) => {
    if (this.state !== PlayerState.DEAD) {
      const newX = this.sprite.x + this.dirX * this.speed * dt * 0.001;
      const newY = this.sprite.y + this.dirY * this.speed * dt * 0.001;

      this.sprite.position.x = trunc(newX, this.r, GAME_WIDTH - this.r);
      this.sprite.position.y = trunc(newY, this.r, GAME_HEIGHT - this.r);
    }

    if (this.state === PlayerState.INVINCIBLE) {
      this.counter += dt;

      if (this.counter >= 100) {
        this.counter -= 100;
        this.toggle = !this.toggle;

        this.sprite.alpha = this.toggle ? 0.8 : 0.1;
      }
    }
  };

  handleInput = () => {
    if (isKeyPressed(KeyCode.UP)) {
      this.dirX = 0;
      this.dirY = -1;

      if (this.state === PlayerState.VULNERABLE) {
        this.changeSpriteFrames(walkFrames);
      }
    } else if (isKeyPressed(KeyCode.RIGHT)) {
      this.dirX = 1;
      this.dirY = 0;

      if (this.state === PlayerState.VULNERABLE) {
        this.changeSpriteFrames(walkFrames);
      }
      this.changeSpriteDir(1);
    } else if (isKeyPressed(KeyCode.DOWN)) {
      this.dirX = 0;
      this.dirY = 1;

      if (this.state === PlayerState.VULNERABLE) {
        this.changeSpriteFrames(walkFrames);
      }
    } else if (isKeyPressed(KeyCode.LEFT)) {
      this.dirX = -1;
      this.dirY = 0;

      if (this.state === PlayerState.VULNERABLE) {
        this.changeSpriteFrames(walkFrames);
      }
      this.changeSpriteDir(-1);
    } else {
      this.dirX = 0;
      this.dirY = 0;

      if (
        this.state === PlayerState.VULNERABLE &&
        this.sprite.textures !== spawnFrames
      ) {
        this.changeSpriteFrames(idleFrames);
      }
    }
  };

  changeSpriteDir = (dirX: number) => {
    const scaleX = PLAYER_WIDTH / this.sprite.textures[0].width;
    const scaleY = PLAYER_HEIGHT / this.sprite.textures[0].height;
    this.sprite.scale.set(dirX * scaleX, scaleY);
  };

  changeSpriteFrames = (frames: Texture[]) => {
    if (this.sprite.textures === frames) return;

    this.sprite.textures = frames;
    this.sprite.play();
  };

  onHurt = () => {
    this.state = PlayerState.INVINCIBLE;
    this.changeSpriteFrames(hurtFrames);

    setTimeout(() => {
      this.state = PlayerState.VULNERABLE;
      this.changeSpriteFrames(idleFrames);

      this.counter = 0;
      this.toggle = true;
      this.sprite.alpha = 1;
    }, 2000);
  };

  onDead = () => {
    this.state = PlayerState.DEAD;

    this.changeSpriteFrames(deadFrames);
  };
}
