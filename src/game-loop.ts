import { FPS } from './constants';

export const MPF = 1000 / FPS;
const SPF = MPF * 0.001;
const raf = requestAnimationFrame || setTimeout;

export class GameLoop {
  private startTime = 0;
  private lastTime = 0;

  private counter = 0;
  private update: (dt: number) => void;

  constructor(updateFn: (dt: number) => void) {
    this.update = updateFn;
  }

  start = () => {
    this.startTime = Date.now();
    this.lastTime = this.startTime;

    raf(this.run);
  };

  private run = () => {
    const current = Date.now();
    const dt = current - this.lastTime;

    this.counter += dt;
    this.lastTime = current;

    while (this.counter > MPF) {
      this.update(SPF);

      this.counter -= MPF;
    }

    raf(this.run);
  };
}
