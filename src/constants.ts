export const GAME_WIDTH = 640;
export const GAME_HEIGHT = 480;
export const ASPECT_RATIO = GAME_WIDTH / GAME_HEIGHT;
export const FPS = 60;

export enum GameState {
  LOADING = 1,
  TITLE,
  PLAY,
  GAMEOVER
}

export enum EnemyDirection {
  TOP_TO_BOTTOM = 1,
  RIGHT_TO_LEFT,
  BOTTOM_TO_TOP,
  LEFT_TO_RIGHT
}

export enum PlayerState {
  VULNERABLE,
  INVINCIBLE,
  DEAD
}

export const ENEMY_WIDTH = 64;
export const ENEMY_HEIGHT = 64;

export const PLAYER_WIDTH = 64;
export const PLAYER_HEIGHT = 64;

export const MAX_LIFE = 3;

export enum KeyCode {
  ANY = 'Any',
  LEFT = 'ArrowLeft',
  UP = 'ArrowUp',
  RIGHT = 'ArrowRight',
  DOWN = 'ArrowDown'
}
